type TObj = { [key: string]: number };

interface Interface{
	maxCard: 5;
}

class Checker implements Interface{
	maxCard: 5 = 5;
	constructor(){}
	public returnCombination(randomArray: string[]): string{
		// console.log("-------------------------------------");
		// console.log(randomArray);
		let getContext = this;// получение контекста

		const obj1: TObj = {};
		const obj2: TObj = {};
		for(const elem of randomArray){
			const cardValue = elem.slice(0, -1);
			const cardSuite = elem.at(-1)!;
			// console.log(cardValue," ",cardSuite)
			(obj1[cardValue] === undefined) ? obj1[cardValue] = 1 : obj1[cardValue]++;
			(obj2[cardSuite] === undefined) ? obj2[cardSuite] = 1 : obj2[cardSuite]++;
		}

		if(Object.keys(obj1).length === 2){
			if(Object.values(obj1).some(item=>item === 4))
				return "Каре";
			else
				return "Фулл-хаус";
		}else if(Object.keys(obj1).length === 3){
			if(Object.values(obj1).some(item=>item === 3))
				return "Тройка";
			else
				return "Две пары";
		}else if(Object.keys(obj1).length === 4){
			return "Пара";
		}else if(Object.keys(obj1).length === 5){
			if(Object.keys(obj2).length === 1){// если только один вид масти в розданных картах
				const weight = returnWeight(obj1);
				if(weight === 12)
					return "Рояль-флэш";
				if(4 <= weight && weight <= 11)
					return "Стрит-флэш";
				else
					return "Флэш";
			}else{
				return "Старшая карта";
			}
		}
		return "нет вариантов";

//-----------------------------------------------------------
		function returnWeight(obj1: TObj){
			const pointsNum: { [key: string]: number } = {
				"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7,
				"8": 8, "9": 9, "10": 10, "J": 11, "Q": 12, "K": 13,
				"T": 14,
			}
			let sum = 0;
			for(const obj1Key in obj1){
				sum += pointsNum[obj1Key];
			}
			return sum / getContext.maxCard;
		}
	}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
}
// [ 'T♣', '4♥', '8♣', 'Q♣', '5♥' ]
// [ 'T♣', '6♣', 'J♣', 'Q♣', '5♥' ]
// [ '2♠', '6♣', 'J♣', 'Q♣', '9♥' ]

// const checker = new Checker()
// console.log(checker.returnCombination([ '2♠', '6♣', 'J♣', 'Q♣', '9♥' ])) //Старшая карта

export default Checker;