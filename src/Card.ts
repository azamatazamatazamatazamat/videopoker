interface Interface2{
	arrayCard: string[][];
}

class Card implements Interface2{
	arrayCard: string[][] = [];

	constructor(){
		this.arrayCard = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "T"]
		.map(item=>[item + "♥", item + "♦", item + "♣", item + "♠"]);
	}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public returnLen(){
		const lenRow = this.arrayCard.length;
		const lenCol = this.arrayCard[0].length;
		return [lenRow, lenCol];
	}

	public showSuite(strArrayCombination: string[]){
		const newArray = strArrayCombination.map(item=>{
			const rows = item.slice(0, -1);
			const columns = item.at(-1);
			return this.arrayCard[+rows!][+columns!];
		});

		if(newArray.some(item=>item === undefined)){
			console.log("array out of bounds! Please enter correct values");
		}else{
			// console.log(newArray)
			return newArray;
		}
	}
}

export default Card;
