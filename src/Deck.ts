interface Interface{
	// strArrayCombination: string[];
	maxCard: 5;
	lenRow:number;
	lenCol:number;
}

class Deck implements Interface{
	private strArrayCombination: string[] = [];
	maxCard: 5 = 5;
	lenRow:number = 0;
	lenCol:number = 0;

	constructor(lenRow:number,lenCol:number){
		this.lenRow = lenRow;
		this.lenCol = lenCol;
		this.generateRandomCombo();
	}

	public returnArrayPosition(){
		return this.strArrayCombination;
	}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	private generateRandomCombo(){
		for(let i = 0; true; i++){
			const random = this.generateRandomArrayPosition();
			if(this.strArrayCombination.some(item=>item === random)) continue;
			this.strArrayCombination.push(random)
			if(this.strArrayCombination.length === this.maxCard) break;
		}
	}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public changeCombination(...args:number[]){
		const trigger = "-1";// выставляем -1 в элементах массива, которые хотим заменить
		args.forEach(itemIndex=>{
			this.strArrayCombination[itemIndex] = trigger;
		});

		args.forEach(itemIndex=>{
			for(let i = 0; true; i++){
				const random = this.generateRandomArrayPosition();
				if(this.strArrayCombination.some(item=>item === random)) continue;
				this.strArrayCombination[itemIndex] = random;
				if(this.strArrayCombination.some(item=>item !== trigger)) break;
			}
		});
	}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	private generateRandomArrayPosition(){
		const rows = Math.floor(Math.random() * this.lenRow).toString();
		const columns = Math.floor(Math.random() * this.lenCol).toString();
		const random = rows + columns;
		// console.log(random)
		return random;
	}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
}

export default Deck;
